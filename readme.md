for this you need squlite3 and python3
this was done on ubuntu 20.04 but should work in most systems

to install dependencies run 
pip3 requirements.txt


the db should be included if nut open squlite3 and run

CREATE TABLE SAT(
   catalog_number INT NOT NULL PRIMARY KEY,
   classification CHAR,
   launch_year INT,
   launch_num_and_designator VARCHAR);


CREATE TABLE LOC(
   loc_id INT NOT NULL PRIMARY KEY,
   sat_id INT NOT NULL,
   date DATETIME,
   first_deriv_mean FLOAT,
   second_deriv_mean FLOAT,
   drag_term FLOAT,
   elem_set_number INT,
   inclination FLOAT,
   right_asc FLOAT,
   eccentricity FLOAT,
   arg_perigree FLOAT,
   mean_anomaly FLOAT,
   mean_motion FLOAT,
   rev_number_at_epoch INT,
   FOREIGN KEY (sat_id)
   REFERENCES SAT (catalog_number) 
   );

to run the program 
python3 stest.py
