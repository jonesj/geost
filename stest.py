import requests
import json
import configparser
import xlsxwriter
import time
from datetime import datetime
import re
import sqlite3

con = sqlite3.connect('space.db')
c = con.cursor()

class MyError(Exception):
    def __init___(self,args):
        Exception.__init__(self,"my exception was raised with arguments {0}".format(args))
        self.args = args





# See https://www.space-track.org/documentation for details on REST queries
# the "Find Starlinks" query searches all satellites with NORAD_CAT_ID > 40000, with OBJECT_NAME matching STARLINK*, 1 line per sat
# the "OMM Starlink" query gets all Orbital Mean-Elements Messages (OMM) for a specific NORAD_CAT_ID in JSON format

uriBase                = "https://www.space-track.org"
requestLogin           = "/ajaxauth/login"
requestCmdAction       = "/basicspacedata/query" 
requestFindStarlinks   = "/class/tle_latest/NORAD_CAT_ID/>40000/ORDINAL/1/OBJECT_NAME/STARLINK~~/format/json/orderby/NORAD_CAT_ID%20asc"
requestOMMStarlink1    = "/class/omm/NORAD_CAT_ID/"
requestOMMStarlink2    = "/orderby/EPOCH%20asc/format/json"
requestnorad           = "/class/gp/decay_date/null-val/epoch/%3Enow-30/orderby/norad_cat_id/format/json"



# Parameters to derive apoapsis and periapsis from mean motion (see https://en.wikipedia.org/wiki/Mean_motion)

GM = 398600441800000.0
GM13 = GM ** (1.0/3.0)
MRAD = 6378.137
PI = 3.14159265358979
TPI86 = 2.0 * PI / 86400.0

# ACTION REQUIRED FOR YOU:
#=========================
# Provide a config file in the same directory as this file, called SLTrack.ini, with this format (without the # signs)
# [configuration]
# username = XXX
# password = YYY
# output = ZZZ
#
# ... where XXX and YYY are your www.space-track.org credentials (https://www.space-track.org/auth/createAccount for free account)
# ... and ZZZ is your Excel Output file - e.g. starlink-track.xlsx (note: make it an .xlsx file)

# Use configparser package to pull in the ini file (pip install configparser)
config = configparser.ConfigParser()
config.read("./SLTrack.ini")
configUsr = config.get("configuration","username")
configPwd = config.get("configuration","password")
configOut = config.get("configuration","output")
siteCred = {'identity': configUsr, 'password': configPwd}

# User xlsxwriter package to write the xlsx file (pip install xlsxwriter)
workbook = xlsxwriter.Workbook(configOut)
worksheet = workbook.add_worksheet()
z0_format = workbook.add_format({'num_format': '#,##0'})
z1_format = workbook.add_format({'num_format': '#,##0.0'})
z2_format = workbook.add_format({'num_format': '#,##0.00'})
z3_format = workbook.add_format({'num_format': '#,##0.000'})


with requests.Session() as session:
    resp = session.post(uriBase + requestLogin, data = siteCred)
    if resp.status_code != 200:
        raise MyError(resp, "POST fail on login")
    resp = session.get(uriBase + requestCmdAction + requestnorad)
    
    if resp.status_code != 200:
        # If you are getting error 500's here, its probably the rate throttle on the site (20/min and 200/hr)
            # wait a while and retry
        print(resp)
        raise MyError(resp, "GET fail on request for Starlink satellite " )
    print(resp)


    retData = json.loads(resp.text)
    noradID = '0'

    for i in retData:
        c.execute("SELECT EXISTS (SELECT 1 FROM SAT WHERE catalog_number =" + i['NORAD_CAT_ID'] + " )")
        c.execute('''INSERT INTO SAT (catalog_number, classification, launch_year, launch_num_and_designator)  VALUES (?, ?, ?, ?);''', ( i['NORAD_CAT_ID'],  i['CLASSIFICATION_TYPE'], i['LAUNCH_DATE'], i['OBJECT_ID'] ))
    con.commit() 
    while(noradID != 'q'):
        noradID = input("what norad ID do you want type q to quit:\n\n")
        for i in retData:
            if(noradID == i['OBJECT_ID']):
                TLE = i['TLE_LINE0'] + i['TLE_LINE1'] + i['TLE_LINE2']
                print('2 line element = ' + TLE + '\n')
                formatDate = re.sub("T"," ",i['EPOCH'])
                #date = (i['EPOCH'].year + '/' + i['EPOCH'].month + '/' + i['EPOCH'].day + ' ' + i['EPOCH'].hour + '-' + i['EPOCH'].minute + '-' + i['EPOCH'].second + '.' + i['EPOCH'].microsecond )
                print('epoch = ' + formatDate + '\n')
        print('\n')
    session.close()
workbook.close()
print("Completed session") 
